$(function () {
    $('#sp_btn').on('click',function(e){
        $('.head_nav_sp').toggleClass('active');
        $('header').toggleClass('active');
        $('.content_wrap').toggleClass('active');
        $('.side_wrap').toggleClass('active');
        $('.sp_btn').toggleClass('close');
        e.stopPropagation();
    });
    $('.link_remove').on('click',function(e){
        $('.head_nav_sp').removeClass('active');
        $('header').removeClass('active');
        $('.content_wrap').removeClass('active');
        $('.side_wrap').removeClass('active');
        $('.sp_btn').removeClass('close');
        e.stopPropagation();
    });
    $(window).resize(function () {
        if ($('.sp_btn').hasClass('close')) {
            $('.head_nav_sp').removeClass('active');
            $('header').removeClass('active');
            $('.content_wrap').removeClass('active');
            $('.side_wrap').removeClass('active');
            $('.sp_btn').removeClass('close');
        }
    });
});

$(function(){
    $('a[href^="#"]').click(function(){
      let speed = 500;
      let href= $(this).attr("href");
      let target = $(href == "#" || href == "" ? 'html' : href);
      let position = target.offset().top;
      $("html, body").animate({scrollTop:position}, speed, "swing");
      return false;
    });
});

function header(rootDir){
    $.ajax({
        url: rootDir + "header.html", // 読み込むHTMLファイル
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //header.htmlの{$root}を置換
            document.write(html);
        }
    });
}

// number
function spinner(counter){
    var step = 1;
    var min = 1;
    var max = 100;

    var c_counter = $(".c_counter").val();
    var c_counter = parseInt(c_counter);
      if (counter == 'up') { c_counter += step; };
      if (counter == 'down') { c_counter -= step; };

    if ( c_counter < min ) { c_counter = min; };
    if ( max < c_counter ) { c_counter = max; };

    $(".c_counter").val(c_counter);
}
