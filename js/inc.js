function header(rootDir){
    $.ajax({
        url: rootDir + "header.html", // 読み込むHTMLファイル
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //header.htmlの{$root}を置換
            document.write(html);
        }
    });
}

function sidebar(rootDir){
    $.ajax({
        url: rootDir + "sidebar.html", // 読み込むHTMLファイル
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //sidebar.htmlの{$root}を置換
            document.write(html);
        }
    });
}

function footer(rootDir){
    $.ajax({
        url: rootDir + "footer.html", // 読み込むHTMLファイル
        cache: false,
        async: false,
        dataType: 'html',
        success: function(html){
            html = html.replace(/\{\$root\}/g, rootDir); //footer.htmlの{$root}を置換
            document.write(html);
        }
    });
}
